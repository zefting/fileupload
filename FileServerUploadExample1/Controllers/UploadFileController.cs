﻿using System;
using System.IO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;

namespace FileServerUploadExample1.Controllers
{
    [Route("api/uploadFile")]
    [ApiController]
    public class UploadFileController : ControllerBase
    {
        [HttpPost, DisableRequestSizeLimit]
        public IActionResult Upload()
        {
            try
            {
                var file = Request.Form.Files[0];
                string folderName = Path.Combine("StaticFiles", "Uploads");
                string pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);
                
                if(file.Length > 0)
                {
                    string fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim().ToString(); 
                    var newFileName = new Guid();
                    string fullPath = Path.Combine(pathToSave, newFileName.ToString());
                    string dbPath = Path.Combine(folderName, newFileName.ToString());
                    // guid uuid 
                    using (var stream = new FileStream(fullPath, FileMode.Create))
                    {
                        file.CopyTo(stream);
                    }
                    
                    
                    return Ok(new { dbPath });
                }

                else
                {
                    return BadRequest();
                }
            }
            catch(Exception ex)
            {
                return StatusCode(500, $"Internal server error: {ex}");
            }
        }
    }
}
